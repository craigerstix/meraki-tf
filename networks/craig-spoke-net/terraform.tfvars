org_id = "1280364"

switch_serials = ["Q3EA-239B-ZQV7"]

switch_map = {
    "craig-spoke-sw1" = {
        serial = "Q3EA-239B-ZQV7"
        trunk_ports = ["23", "24"]
        access_ports = ["2", "3"]
    }
}

ap_serials = ["Q5AC-AQBB-Q74N"]

ap_map = {
    "craig-spoke-ap1" = "Q5AC-AQBB-Q74N"
}

mx_serials = ["Q2KY-B8M4-BXR2"]

mg_serials = ["Q2YY-UKJ3-W9AM"]
