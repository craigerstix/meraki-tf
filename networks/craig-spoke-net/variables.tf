variable "org_id" {
    type = string
}

variable "switch_serials" {
    type = list
}

variable "switch_map" {
    type = map(object({
        serial = string
        trunk_ports = list(string)
        access_ports = list(string)
    }))
}

variable "ap_serials" {
    type = list
}

variable "ap_map" {
    type = map(string)
}

variable "mx_serials" {
    type = list
}

variable "mg_serials" {
    type = list
    default = []
}
