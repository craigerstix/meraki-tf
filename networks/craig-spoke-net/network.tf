resource meraki_networks "craig-spoke-net" {
    organization_id = var.org_id
    name = "craig-spoke-net"
    product_types = ["appliance", "cellularGateway", "switch", "wireless"]
    tags = ["terraform"]
}

locals {
  serials = concat(var.mx_serials, var.switch_serials, var.ap_serials, var.mg_serials)
}

data meraki_devices "current_devices" {
    network_ids =[meraki_networks.craig-spoke-net.id]
    organization_id = var.org_id
}

locals  {
  new_devices = setsubtract(toset(local.serials), toset(try(data.meraki_devices.current_devices.items[*].serial, []))) 
}

#output "new_devices" {
#    value = local.new_devices
#}

resource meraki_networks_devices_claim "craig-spoke-net-devices" {
    network_id = meraki_networks.craig-spoke-net.id
    parameters = {
        serials = local.new_devices
    }
}