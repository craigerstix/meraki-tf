

resource "meraki_networks_switch_access_policies" "test_policy" {
    network_id = "L_3695766444210915322"
    name = "tf-test-policy"
    access_policy_type = "802.1x"
    host_mode = "Single-Host"
    radius_servers = [ {
        host = "172.17.1.153"
        port = 1812
        secret = "myisepsk!"
    } ]
    radius_testing_enabled = false
    radius_coa_support_enabled = true
    radius_accounting_enabled = false
    url_redirect_walled_garden_enabled = false
}


resource "meraki_devices_switch_ports" "craig-spoke-net-access" {
    port_id = "2"
    serial = "Q3EA-239B-ZQV7"
    type = "access"
    name = "tf-access"
    vlan = "10"
    access_policy_type = "Custom access policy"
    access_policy_number =  meraki_networks_switch_access_policies.test_policy.access_policy_number
    enabled = true
}