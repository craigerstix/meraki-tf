# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "hashicorp.com/edu/meraki" {
  version     = "0.1.0-alpha"
  constraints = "0.1.0-alpha"
  hashes = [
    "h1:AFhBYJr4fV6wPhGtzqLBHQOec4BffnbSa20A/aXZitY=",
  ]
}
